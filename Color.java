package ev3;

import lejos.hardware.BrickFinder;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.TextLCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.SensorMode;

public class Color {
	
	public static int color() {
		/* 
		 * wei� = 1
		 * schwarz = 2
		 * rot = 3
		 * blau = 4
		 * gelb = 5
		 * gr�n = 6 
		 *
		 * Default werte | | mx = max | mn = min
		  
		final int[] white_mx = {21,21,21};
		final int[] white_mn = {19,19,19};
		
		final int[] black_mx = {0,0,0};
		final int[] black_mn = {1,1,1};
		
		final int[] red_mx = {20,4,3};
		final int[] red_mn = {13,4,3};
		
		final int[] blue_mx = {1,4,11};
		final int[] blue_mn = {0,4,7};
		
		final int[] yellow_mx = {10,6,3};
		final int[] yellow_mn = {9,5,1};
		
		final int[] green_mx = {1,3,2};
		final int[] green_mn = {0,2,1}; 
		 
		 */
		
		final int[] white_mx = {21,21,21};
		final int[] white_mn = {19,19,19};
		
		final int[] black_mx = {0,0,0};
		final int[] black_mn = {1,1,1};
		
		final int[] red_mx = {20,4,3};
		final int[] red_mn = {13,4,3};
		
		final int[] blue_mx = {1,4,11};
		final int[] blue_mn = {0,4,7};
		
		final int[] yellow_mx = {10,6,3};
		final int[] yellow_mn = {9,5,1};
		
		final int[] green_mx = {1,3,2};
		final int[] green_mn = {0,2,1};
		
		//---------------------------------------------------------//
		
		final EV3 ev3 = (EV3) BrickFinder.getLocal();	
		TextLCD lcd = ev3.getTextLCD();
		int color_r = 1;

		// Color sensor
		EV3ColorSensor colorSensor = new EV3ColorSensor(SensorPort.S1);
		SensorMode color = colorSensor.getRGBMode();
		float[] colorSample = new float[color.sampleSize()];
		lcd.drawInt(colorSample.length, 0, 2);	
	
		color.fetchSample(colorSample, 0);
		lcd.drawString("" + (colorSample[0]*100), 0, 3);
		lcd.drawString("" + (colorSample[1]*100), 0, 4);
		lcd.drawString("" + (colorSample[2]*100), 0, 5);
	
		/* 
		 * berechnet aus den Messwerten die Farbe. es bei gilt
		 * wei� = 1
		 * schwarz = 2
		 * rot  = 3
		 * blau = 4
		 * gelb = 5
		 * gr�n = 6 
		 */
		
			 if(selectcolor(colorSample,white_mn,white_mx)) 	{color_r = 1;}
		else if(selectcolor(colorSample,black_mn,black_mx)) 	{color_r = 2;}
		else if(selectcolor(colorSample,red_mn,red_mx)) 		{color_r = 3;}
		else if(selectcolor(colorSample,blue_mn,blue_mx)) 	 	{color_r = 4;}
		else if(selectcolor(colorSample,yellow_mn,yellow_mx))	{color_r = 5;}
		else if(selectcolor(colorSample,green_mn,green_mx)) 	{color_r = 6;}
		
		colorSensor.close();
		return color_r;
	}

	private static boolean selectcolor(float[] scan, int[] max, int[] min) {
		
			boolean[] color = {false,false,false};
			boolean color_r = false;
			int m;
			
			// toleranz f�r Fabr sensor durch vergleicheh mit Interval 
			
			for(int i = 0; i <= 2;i++) {
			m = (min[i]-max[i])/2;
			if(Math.abs(scan[i]-m) <= Math.abs(min[i]-m)){color[i] = true;}
			}
			
			if(color[0] == color[1] && color[1] == color[2]) {color_r = true;}
			
			return color_r;
	}

}




